function get_array() {
    var arr = [];
    for(i=1; i<=255; i++){
        arr.push(i);
    }
    return arr; 
}

console.log(get_array());

// 

// get even 1000

function get_even_number(){
    var sum = 0;
    for(var i = 1; i < 1001; i++){
        if(i % 2 === 0){
            sum += i
        }
    }
    return sum;
}


// get odd numbers 5000

function get_odd_5000(){
    var sum = 0;
    for(var i = 1; i < 5001; i++){
        if(i % 2 === 1){
            sum += i
        }
    }
    return sum;
}

// iterate an array
// Write a function that returns the sum of all the values within an array. (e.g. [1,2,5] returns 8, [-5,2,5,12] returns 14)

function iterArr(arr) {
    var sum = 0;
    for(var i = 0; i <arr.length; i++){
        sum = sum + arr[i];
    }
    return sum;
}

// find max
// Given an array with multiple values, write a function that returns the maximum number in the array. (e.g. for [-3,3,5,7] max is 7)

function findMax(arr) {
    var max = arr[0];

    for (var i=1; i<arr.length; i++) {
        if (max < arr[i]) {
            max = arr[i]
        }
    }
    return max;
}

// find average in a given array

function findAvg(arr) {
    var sum = 0;
    for(var i = 0; i < arr.length; i++) {
        sum = sum + arr[i];
    }
    return sum / arr.length;
}

// find odd in a given array

function findOdd(arr){
    var array = []
    for(var i = 0; i < 50; i++){
        if(i % 2 !== 0){
            array.push(i)
        }
    }
    return array;
}

// greater than Y

function greaterY(arr, Y) {
    var count = 0;
    for(var i = 0; i < arr.length; i++){
        if(arr[i] > Y) {
            count++;
        }
    }
    return count;
}

// squares - creating squares in an array

function squareVal(arr){
    for(var i = 0; i < arr.length; i++){
        arr[i] = arr[i]*arr[i];
    }
        return arr;
}

// negetives

function noNeg(arr){
    for (var i = 0; i < arr.length; i++) {
        if(arr[i] < 0) {
           arr[i] = 0
        }
    }
    return arr;
}

// max/min/avg
// Given an array with multiple values, write a function that returns a new array that only contains the maximum, minimum, and average values of the original array. (e.g. [1,5,10,-2] will return [10,-2,3.5])

function maxMinAvg(arr) {
    var max = [0];
    var min = [0];
    var sum = [0];

    for (var i = 0; i < arr.length; i++) {
        if (arr[i] > max) {
            max = arr[i];
        }
        if(arr[i] < min) {
            min = arr[i];
        }
        sum = sum + arr[i];
    }
    var avg = sum / arr.length;
    var arrnew = [max, min, avg];
    return arrnew;
}


// swap values
// Write a function that will swap the first and last values of any given array. The default minimum length of the array is 2. (e.g. [1,5,10,-2] will become [-2,5,10,1]).

function swap(arr) {
    var temp = arr[0];
    arr[0] = arr[arr.length - 1];
    arr[arr.length -1] = temp;
    return arr;
}

// number to string
// Write a function that takes an array of numbers and replaces any negative values within the array with the string 'Dojo'. For example if array = [-1,-3,2], your function will return ['Dojo','Dojo',2].

function numToStr(arr){
    for (var i = 0; i < arr.length; i++) {
        if (arr[i] < 0){
            arr[i] = 'Dojo'
        }
    }
    return arr;
}

x = -1
msglen = 9



output =  ,0,
