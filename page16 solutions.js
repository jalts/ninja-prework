// Setting and Swapping
var myNumber = 42;
var myName = "Devon";

// use a "temp" variable to handle the swap
var temp = myNumber;

myNumber = myName;
myName = temp;

// Print -52 to 1066
//------------------------------------
for(var i=-52; i<=1066; i++) {
    console.log(i);
}

// Don't Worry, Be Happy
//------------------------------------

// define the function
function beCheerful() {
    console.log("good morning!");
}

// call it 98 times!
for(var i=0; i<98; i++) {
    beCheerful();
}

// Multiples of Three – but Not All
//------------------------------------

// Using FOR, print multiples of 3 from -300 to 0.
for(var i=-300; i<=0; i++) {
    
    // check if i is evenly divisible by 3
    if(i%3==0) {
        // skip -3 and -6
        if(i === -3 || i === -6) {
            console.log(i);
        }
    }
}

// Printing Integers with While
//------------------------------------

// Print integers from 2000 to 5280,using a WHILE.
var num = 2000;
while(num <= 5280) {
    console.log(num);
    num += 1;
}

// You Say It’s Your Birthday
//------------------------------------

var devonsBirthMonth = 6;
var devonsBirthDay = 2;

// If 2 given numbers represent your birth month and day in either order, log "How did you know?"
function birthdayCheck(day, month, testA, testB) {
    if((day === testA && month == testB) || (day === testB && month === testA)) {
        console.log("How did you know?");
    } else {
        // else log "Just another day...."
        console.log("Just another day...");
    }
}

// call function with provided birthdates and two test numbers
birthdayCheck(devonsBirthDay, devonsBirthMonth, 2, 5);

// Leap  Year
//------------------------------------
// Write a function that determines whether a given year is a leap year.
// https://www.w3resource.com/w3r_images/javascript-basic-image-exercise-6.png
function isLeapYear(year) {
    if(year%100 === 0) {
        if(year % 400 === 0) {
            return true;
        }
        else {
            return false;
        }
    }
    else if(year % 4 === 0) {
        return true;
    }
    else {
        return false;
    }
}
console.log(isLeapYear(2020));

// Print and Count
//------------------------------------
// Print all integer multiples of 5,from 512 to 4096

var total = 0;
for(var i=512; i<= 4096; i++) {
    if(i % 5 === 0) {
        console.log(i);
        total++;
    }
}
// Afterward, also log how many there were
console.log("There were " + total + " multiples of 5");

// Multiples of Six
//------------------------------------
// Print multiples of 6 up to 60,000, using a WHILE
var sixes = 0;
while(sixes <= 60000) {
    if(sixes % 6 === 0) {
        console.log(sixes);
    }
    sixes++;
}

// Counting, the Dojo Way
//------------------------------------
// Print integers 1 to 100.
// If divisible by 5, print "Coding" instead.
// If by 10, also print " Dojo"
for(var i=1; i<=100; i++){
    if(i%10 === 0) {
        console.log("Coding Dojo");
    }
    else if(i%5 === 0) {
        console.log("Coding");
    }
    else {
        console.log(i);
    }
}

// What do you Know?
//------------------------------------
// Your function will be given an input parameter incoming.
function whatDoYouKnow(incoming) {
    console.log(incoming);
}
// Please console.log this value


// Whoa,That Sucker’s Huge...
//------------------------------------
// Add odd integers from -300,000 to 300,000 and console.log the final sum.
// Is there a shortcut?

// shortcut: incrementing by 2, starting on odd number. guaranteed odd numbers!
// also shortcut (should always be 0 anyway?)
var sums = 0;
for(var i=-300000; i<=300000; i+=2) {
    sums += i;
}
console.log(sums)


// Countdown by Fours
//------------------------------------
// Log positive numbers starting at 2016, counting down by fours (exclude 0), without a FOR loop.
var fours = 2016;
while(fours > 0) {
    console.log(fours);
    fours -= 4;
}

// Flexible Countdown
//------------------------------------
// Based on earlier “Countdown by Fours”,
// given lowNum, highNum, mult
function flexibleCountdown(lowNum, highNum, mult) {
    // print multiples of mult from highNum down to lowNum using a FOR.
    for(var i=highNum; i>lowNum; i-=1) {
        if(i%mult === 0) {
            console.log(i);
        }
    }
}
flexibleCountdown(2, 9, 3);

// Final Countdown
//------------------------------------
// This is based on “Flexible Countdown”.
// The parameter names are not as helpful, but the problem is essentially identical; don’t be thrown off!
// Given 4 parameters (param1,param2,param3,param4), print the multiples of param1, starting at param2 
// and extending to param 3.
function finalCountdown(param1, param2, param3, param4) {
    var num = param2;
    // Dothis using a WHILE.
    while(num <= param3) {
        if(num%param1 === 0) {
            // One exception: if a multiple is equal to param4, then skip (don’t print) it.
            if(num !== param4) {
                console.log(num);
            }
        }
        num++;
    }
}
//Given (3,5,17,9), print 6,12,15 (which are all of the multiples of 3 between 5 and 17,and excluding the value 9)
finalCountdown(3,5,17,9);