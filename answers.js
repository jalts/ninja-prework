//Setting and Swapping
var myNumber = 42
var myName = James
var temp = myNumber

myNumber = myName
myName = temp

//Print -52 to 1066
for (var i = -52; i < 1067; i++){
    console.log(i)
}

//don't worry, be happy
function beCheerful(){
    var x = "good morning!"
    for (var i = 0; i < 99; i++){
      console.log(x);
    }
}
 
  console.log(beCheerful());

  //multiples of Three - but not all 
for (var i=-300; i<=0; i++) {
    if(i%3==0) {                        // if multiples of 3
        if(i === -3 || i === -6) {      //skipping -3 and -6
            console.log(i);
        }
    }
}

//printing ints with while

var x = 2000;

while (x < 5281){
    console.log(x);
    x = x + 1;
}

//you say its your birthday
// want to run through with this with a TA, I get most of what's going on - just would be nice to review 

var bDay = 21
var bMonth = 7

function birthdayCheck(bDay, bMonth, testA, testB) {
    if((bDay === testA && bMonth === testB) || (bDay === testB && bMonth === testA)) {
        console.log('How did you know?');
    } else {
        console.log('Just another day...');
    }
}

//leap year

function isLeapYear(year) {
    if(year%100 === 0) {
        if(year % 400 === 0) {
            return true;
        }
        else {
            return false;
        }
    }
    else if(year % 4 === 0) {
        return true;
    }
    else {
        return false;
    }
}
console.log(isLeapYear(2020));

//print and count

var total = 0;
for(var i=512; i<= 4096; i++) {
    if(i % 5 === 0) {
        console.log(i);
        total++;
    }
}
console.log("There were " + total + " multiples of 5");

//multiples of six

var sixes = 0;
while(sixes <= 60000) {
    if(sixes % 6 === 0) {
        console.log(sixes);
    }
    sixes++;
}

//counting, the dojo way

for(x = 1; x < 101; x++){
    if(x % 5 === 0){
        console.log('Coding Dojo')
    }
    else if(x % 10 === 0){
        console.log('Dojo');
    }
    else{
    		console.log(x)
    }
}

//what do you know?

function whatDoYouKnow(incoming){
    console.log(incoming);
}

//whoa, that sucker's huge...
var sums = 0;
for(var i=-300000; i<=300000; i+=2) {
    sums += i;
}
console.log(sums)

//countdown by fours

var x = 2016;
while(x > 0){
   console.log(x);
   x = x - 4
}

//flexible countdown

function flexibleCountdown(lowNum, highNum, mult) {
    for(var i=highNum; i>lowNum; i-=1) {
        if(i%mult === 0) {
            console.log(i);
        }
    }
}
flexibleCountdown(2, 9, 3);

//the final countdown

function finalCountdown(param1, param2, param3, param4) {
    var num = param2;
    while(num <= param3) {
        if(num%param1 === 0) {
            if(num !== param4) {
                console.log(num);
            }
        }
        num++;
    }
}
finalCountdown(3,5,17,9);

